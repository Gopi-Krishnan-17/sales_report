import { useState } from 'react';
import './App.css';
import NewSales from './Components/NewSales/NewSales';
import Sold from './Components/SoldItems/Sold';

const first_sold = [
  {
    id: 's1',
    name: 'Yamaha',
    date: new Date(2021, 5,12),
    amount:'110000',
  },
  {
    id: 's2',
    name: 'Honda',
    date: new Date(2022, 6,28),
    amount:'90000',
  },
  {
    id: 's3',
    name: 'Suzuki',
    date: new Date(2019, 3,1),
    amount:'88000',
  },
  {
    id: 's4',
    name: 'TVS',
    date: new Date(2021, 4,19),
    amount:'70000',
  },
  {
    id: 's5',
    name: 'Royal Enfield',
    date: new Date(2019, 8,10),
    amount:'150000',
  }
]
const App = () =>
{
  const [sold,setSold]=useState(first_sold);

  const addSalesHandler=(sold)=>{
    setSold((prevSold)=>{
      return[sold,...prevSold];
    })
  }
  return (
    <div>
      <NewSales onAddSale={addSalesHandler}/>
      <Sold items={sold}/>
    </div>
  );
}

export default App;
