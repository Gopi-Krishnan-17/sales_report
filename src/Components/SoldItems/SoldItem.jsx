import React from 'react';
import './SoldItem.css';
import SoldDate from './SoldDate.jsx';
const SoldItem = (props) =>
{
    return(
        <div>
            <div className="sold-bike">
            <SoldDate date={props.date}/>
            <div className="sold-bike_name"><h2>{props.name}</h2></div>
            <div className="sold-bike_price">Rs. {props.amount}</div>
            </div>
        </div>
    );
}
export default SoldItem;
