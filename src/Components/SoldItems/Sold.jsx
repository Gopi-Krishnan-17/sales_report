import React, {useState}from "react";
import SoldFilter from "./SoldFilter";
import SoldItem from "./SoldItem";
import SalesChart from "./SalesChart";
const Sold = (props)=>
{
    const [filteredYear,setFilteredYear]=useState('2019');
    const filterChangeHandler=selectedYear =>{
        setFilteredYear(selectedYear);
    }
    const filteredSales = props.items.filter(sold=>{
        return sold.date.getFullYear().toString()===filteredYear;
    })

    let salesContent = <p style={{textAlign:"center",fontWeight:"bold",fontSize:"2rem"}}>No Sales Record Found</p>
    if(filteredSales.length>0){
        salesContent =  filteredSales.map(sold=> 
            <SoldItem 
            key={sold.id}
            name={sold.name} 
            amount={sold.amount} 
            date={sold.date}
            />
            )
    }
    return(
        <div>
            <SoldFilter selected={filteredYear} onChangeFilter={filterChangeHandler}/>
            <SalesChart sold = {filteredSales}/>
            {salesContent}
        </div>
    )
}
export default Sold;
