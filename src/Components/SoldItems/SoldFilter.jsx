import React from "react";
import './SoldFilter.css';
const SoldFilter = (props)=>
{
    const dropdownChangeHandler = (event)=>
    {
        props.onChangeFilter(event.target.value);
    }
return(
    <div className="dropdown">
        <label>Filter by Year</label>
        <select value={props.selected} onChange={dropdownChangeHandler}>
        <option value='2019'>2019</option>
        <option value='2020'>2020</option>
        <option value='2021'>2021</option>
        <option value='2022'>2022</option>
        </select>
    </div>
)
}
export default SoldFilter;
