import React from "react";
import './NewSales.css';
import NewSalesForm from "./NewSalesForm";
const NewSales = (props) =>
{
    const savesoldDataHandler=(enteredsoldData)=>{
        const soldData={
            ...enteredsoldData,
            id:Math.random().toString()
        };
        props.onAddSale(soldData);
    }
    return(
        <NewSalesForm onSavesoldData={savesoldDataHandler}/>
    )
}
export default NewSales;
