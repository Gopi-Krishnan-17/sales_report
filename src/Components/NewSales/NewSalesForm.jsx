import React , {useState} from "react";
import './NewSalesForm.css';
const NewSalesForm = (props) =>
{
    const[enteredName,setEnteredName] = useState();
    const[enteredAmount,setEnteredAmount]=useState();
    const[enteredDate,setEnteredDate]=useState();

    const NameChangeHandler=(event)=>{
        setEnteredName(event.target.value);
    }
    const AmountChangeHandler=(event)=>{
        setEnteredAmount(event.target.value);
    }
    const DateChangeHandler=(event)=>{
        setEnteredDate(event.target.value);
    }
    const SubmitHandler=(event)=>{
        event.preventDefault();
        
        const soldData={
            name:enteredName,
            amount:enteredAmount,
            date:new Date(enteredDate)
        };
        props.onSavesoldData(soldData);
        setEnteredName('');
        setEnteredAmount('');
        setEnteredDate('');
        };
    return(
    <div className="new-sales-total"> 
    <form onSubmit={SubmitHandler}>
                <div className="new-sales-ind">
                    <div className="new-sales-ind1">
                    <label>Name</label>
                    <input type='text' value={enteredName} onChange={NameChangeHandler}/>
                    </div>
                </div>  
                <div className="new-sales-ind">
                    <div className="new-sales-ind2">
                    <label>Amount</label>
                    <input type='number' value={enteredAmount} onChange={AmountChangeHandler}/>
                    </div>
                </div>
                <div className="new-sales-ind">
                    <div className="new-sales-ind3">
                    <label>Date</label>
                    <input type='date' max='2022-12-31'value={enteredDate} onChange={DateChangeHandler}/>
                    </div>
                </div>
                <div className="actions">
                    <button type="submit">Add Sold Bike</button></div>
    </form>
    </div>
    );
}
export default NewSalesForm;
